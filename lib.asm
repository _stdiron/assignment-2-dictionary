%define code_exit 60
%define code_print 1
%define code_read 0
%define ascii_nums_start "0"
%define ascii_nums_end "9"
%define ascii_minus "-"

global exit
global print_string
global print_error
global string_length
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_copy 

section .text 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, code_exit
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, -1
    .count_loop:
        inc rax
        test byte[rdi+rax], -1
        jnz .count_loop
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rdi, 1
    mov rax, code_print
    syscall    
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_error:
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rdi, 2
    mov rax, code_print
    syscall    
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1
    mov rdi, 1
    mov rsi, rsp
    mov rax, code_print
    syscall 
    pop rdi   
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, 10
    mov r8, rsp
    dec rsp
    mov byte[rsp], 0
    .loop:
        cmp rax, 10
        jb .end
        xor rdx, rdx
        div rcx
        dec rsp
        mov byte[rsp], dl
        add byte[rsp], ascii_nums_start
        jmp .loop
    .end:
        dec rsp
        mov byte[rsp], al
        add byte[rsp], ascii_nums_start
        mov rdi, rsp
        call print_string
        mov rsp, r8
        ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .print

    push rdi
    mov rdi, ascii_minus
    call print_char
    pop rdi
    neg rdi

    .print:
        call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r8, r8
    .loop:
        test byte[rdi+r8], -1
        je .check_sec
        mov al, byte[rsi+r8]
        cmp byte[rdi+r8], al
        jne .not_eq
        inc r8
        jmp .loop
    
    .check_sec:
        test byte[rsi+r8], -1
        je .eq
        jmp .not_eq
    .eq:
        mov rax, 1
        ret
    .not_eq:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, code_read
    mov rdi, 0
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor r8, r8 ; word length
    push 1
    dec rsi
    ; xor rcx, rcx
    .loop:
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        ; check if sdin ended
        test rax, rax
        je .end
        ; check if space checks needed
        test byte[rsp], -1
        je .skip
        ; space checks
        cmp rax, 0x20
        je .loop
        cmp rax, 0x9
        je .loop
        cmp rax, 0xA
        je .loop
        mov byte[rsp], 0
        .skip:
            cmp rax, 0x20
            je .end
            cmp rax, 0x9
            je .end
            cmp rax, 0xA
            je .end
            ; writing a char
            cmp rsi, r8
            je .error
            mov byte[rdi+r8], al
            inc r8
            jmp .loop
    .end:
        add rsp, 8
        mov byte[rdi+r8], 0x00
        mov rax, rdi
        mov rdx, r8
        ret
    .error:
        add rsp, 8
        xor rax, rax
        ret

; read whole line with spaces between words
read_line:
    xor r8, r8 ; word length
    push 1
    dec rsi
    ; xor rcx, rcx
    .loop:
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        ; check if sdin ended
        test rax, rax
        je .end
        ; check if space checks needed
        test byte[rsp], -1
        je .skip
        ; space checks
        cmp rax, 0x20
        je .loop
        cmp rax, 0x9
        je .loop
        cmp rax, 0xA
        je .loop
        mov byte[rsp], 0
        .skip:
            ; check newline
            cmp rax, 0xA
            je .end
            ; writing a char
            cmp rsi, r8
            je .error
            mov byte[rdi+r8], al
            inc r8
            jmp .loop
    .end:
        add rsp, 8
        mov byte[rdi+r8], 0x00
        mov rax, rdi
        mov rdx, r8
        ret
    .error:
        add rsp, 8
        xor rax, rax
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    mov rcx, 10
    .loop:
        xor r8, r8
        mov r8b, [rdi+rdx]
        cmp r8b, 0
        je .end 
        cmp r8b, ascii_nums_start
        jb .end
        cmp r8b, ascii_nums_end
        ja .end

        push rdx
        mul rcx
        pop rdx

        sub r8b, ascii_nums_start
        add rax, r8

        inc rdx 
        jmp .loop
    .end:
        ret
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push 1
    cmp byte[rdi], ascii_minus
    jne .parse
    mov byte[rsp], 0
    inc rdi
    .parse:
        call parse_uint
    test rdx, rdx
    je .error
    test byte[rsp], -1
    je .neg
    add rsp, 8
    ret
    .neg:
        neg rax
        inc rdx 
        add rsp, 8
        ret
    .error:
        add rsp, 8
        ret
        
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    .loop:
        cmp rdx, rcx
        je .error
        mov r8, [rdi+rcx]
        test r8, -1
        je .end
        mov rax, [rdi+rcx]
        mov [rsi+rcx], rax
        inc rcx
        jmp .loop
    .error:
        mov rax, 0
        ret
    .end:
        mov rax, rcx
        ret
