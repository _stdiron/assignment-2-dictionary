%include "lib.inc"

global find_word

section .text

;rdi - string ptr
;rsi - list ptr
find_word:
	.loop:
		test rsi, rsi
		jz .not_found
		push rdi
		push rsi
		
		; add rsi, 8
		; mov rdi, rsi
		; call print_string
		; pop rsi
		; pop rdi
		; push rdi
		; push rsi

		add rsi, 8
		call string_equals
		test rax, rax
		jnz .found
		pop rsi
		pop rdi
		mov rsi, [rsi]
		jmp .loop	
	.not_found:
		mov rax, 0
		ret
	.found:
		pop rsi
		add rsp, 8 
		mov rax, rsi
		ret
