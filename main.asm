%include "lib.inc"
%include "dict.inc"
%define buffsize 255

section .rodata
%include "words.inc"
err_msg_bo: db "ERROR: buffer overflowed", 0
err_msg_nf: db "ERROR: string not found", 0
msg_ik: db "INFO: key value - ", 0
msg_fv: db "INFO: found data - ", 0

global _start

section .bss
buffer: resb buffsize


section .text

_start:
	; read line
	mov rdi, buffer
	mov rsi, buffsize
	call read_line	
	test rax, rax
	jz .size_error
	; print user input
	mov rdi, msg_ik
	call print_string 
	mov rdi, buffer
	call print_string
	call print_newline
	; look for read
	mov rdi, buffer
	mov rsi, LIST_START
	call find_word
	test rax, rax
	jz .not_found
	; look for value ptr in list item
	add rax, 8
	push rax
	mov rdi, rax
	call string_length
	add [rsp], rax
	; print found
	mov rdi, msg_fv
	call print_string
	pop rdi
	inc rdi
	call print_string
	jmp .exit	
	.size_error:
		mov rdi, err_msg_bo
		call print_error
		jmp .exit
	.not_found:
		mov rdi, err_msg_nf
		call print_error
		jmp .exit
	.exit:
		call print_newline
		call exit
