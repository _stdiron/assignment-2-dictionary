ASM=nasm
ASMFLAGS=-felf64
LD=ld

.PHONY: all clean

all: program

clean:
	$(RM) *o

main.asm: lib.inc dict.inc words.inc
	touch main.asm

dict.asm: lib.inc
	touch dict.asm

words.asm: colon.inc
	touch words.asm

	
%.o: %.asm
	$(ASM) $(ASMFLAGS) $< -o $@

program: lib.o dict.o main.o
	$(LD) -o $@ $^ 
