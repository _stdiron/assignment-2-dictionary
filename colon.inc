%define LIST_START 0

%macro colon 2
%2:
	dq LIST_START
	db %1, 0
	%define LIST_START %2
%endmacro
